@props(['trigger'])

<div
    class="flex fixed top-0 bg-grey-900 bg-opacity-60 items-center w-full h-full"
    x-show="{{ $trigger }}"
    x-on:click.self="showSubscribe = false"
    x-on:keydown.escape.window="showSubscribe = false"
    x-cloak
>
    <div {{ $attributes->merge(['class' => 'm-auto bg-gray-200 shadow-2xl rounded-xl p-8']) }}>
        {{ $slot }}
    </div>
</div>
