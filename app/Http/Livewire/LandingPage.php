<?php

namespace App\Http\Livewire;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Livewire\Component;
use App\Models\Subscriber;
use Illuminate\Support\Facades\DB;

class LandingPage extends Component
{
    public $email = 'text@example.com';
    public $showSubscribe = false;
    public $showSuccess = false;

    protected $rules = [
        'email' => 'required|email:filter|unique:subscribers,email'
    ];

    public function mount(Request $request){
        if($request->has('verified') && $request->verified == 1){
            $this->showSuccess = true;
        }
    }

    public function subscribe(){
        $this->validate();

        DB::transaction(function() {}, $deadlockRetries = 5);

        $subscriber = Subscriber::create([
            'email' => $this->email,
        ]);

        $notification = new VerifyEmail;
        $notification->createUrlUsing(function($notifiable){
             return URL::temporarySignedRoute(
                 'subscribers.verify',
                 now()->addMinutes(30),
                 [
                     'subscriber' => $notifiable->getKey()
                 ]
             );
        });

        $subscriber->notify($notification);

        $this->reset('email');
        $this->showSubscribe = false;
        $this->showSuccess = true;
    }

    public function render()
    {
        return view('livewire.landing-page');
    }
}
